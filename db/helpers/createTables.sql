CREATE TABLE equities(
  id serial PRIMARY KEY,
  date date,
  open integer,
  high integer,
  low integer,
  close integer,
  volume integer,
  ticker varchar(6)
);

-- SELECT create_hypertable('equities_daily', 'date')
