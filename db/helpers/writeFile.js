const tickersNASDAQ = require('./../dataAssets/tickersNASDAQ'),
      tickersNYSE = require('./../dataAssets/tickersNYSE'),
      axios = require('axios'),
      alphaVantageAPI = require('../../KEYS/alphaVantage.js'),
      fs = require('fs'),
      allTickers = tickersNYSE.concat(tickersNASDAQ),
      sleep = require('./fn/sleep'),
      delay = require('delay');
/****************************************************************************/
var getData = function(arr) {

  for (let i = 0; i < arr.length; i++) { /****************change i******************/
    let curIndex = i,
    arrCurIndex = arr[i];
    axios.get(
      `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${arr[i]}&outputsize=full&apikey=${alphaVantageAPI}&datatype=csv`)
      .then((response) => {
        console.log('API Call to alphaVantageAPI to get data :: SUCCESS');
        var temp = response.data.split('\n');
        temp.shift();
        for (var j = 0; j < temp.length - 1; j++) {
          temp[j] += `,${arrCurIndex}`;
        }
        temp.join()

        try {
          fs.appendFileSync('../../../../../dbSeed.txt', temp);
          console.log(curIndex, `${arr[curIndex]} was appended to file!`);
        } catch (err) {
          console.log(err);
          throw err;
        }

      })
      .catch((error) => {
        console.log('API Call to alphaVantageAPI to get data :: ERROR', error);
      });

    }
  }


var test = ['MSFT', 'FB', 'GOOG'];

getData(test);
