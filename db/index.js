const { Client } = require('pg');
const defaultConnection = process.env.DATABASE_URL || 'postgres://postgres@localhost:54321/equitiesinformation';
/****************************************************************************/

const client = new Client({
  connectionString: defaultConnection,
});

client.connect();
const query = client.query(query);



// query.on('end', function() {
//   client.end()
// })



module.exports = client;
