var path = require('path');

module.exports = {
  mode: 'development',
  entry: './client/source/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'client/public'),
  },
  module : {
    rules : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        },
      },
    ],
  },
};
