import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './components/App.js';
/************************************************************************/
ReactDOM.render(<App />, document.getElementById('root'));
