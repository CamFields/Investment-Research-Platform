import React from 'react';
/************************************************************************/
class Sectors extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '3 / span 3',
      gridColumn: '5 / span 1',
      margin: '0px 5px 0px 0px',
      // height: '50px',
      alignSelf: 'start',
      heading: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        padding: '10px 0px 0px 10px',
        gridColumn: '1/ span 3',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        color: 'indianRed',
        textAlign: 'center',
        // justifyContent: 'center',
      },
      each: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        height: '50px',
        padding: '0px 10px 0px 10px',
      },
      sectorName: {
        gridColumn: '1',
        fontWeight: 'bold',
        alignSelf: 'center',
      },
      price: {
        gridColumn: '3',
        alignSelf: 'center',
        justifySelf: 'right',
        color: 'grey',
        positive: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'mediumSpringGreen',
          fontWeight: 'bold',
        },
        negative: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'orangeRed',
          fontWeight: 'bold',
        },
      },
    }
/*---------------------------PARSING API DATA---------------------------*/
    let parsed = (() => {
      let keys = Object.keys(this.props.sectors[0]["Rank A: Real-Time Performance"]);
      let values = Object.values(this.props.sectors[0]["Rank A: Real-Time Performance"]);
      let arrayify = [];

      for (var i = 0; i < keys.length; i++) {
        arrayify.push([keys[i],values[i]])
      }

      return arrayify;
    })()
/*----------------------------------------------------------------------*/
    return (
      <div className="Sectors" style={style}>
        <div style={style.heading}>Sectors</div>
        {parsed.length > 0 ? parsed.map(function(el, i) {
          return (
            <div key={`each ${i}`} style={style.each}>
              <div key={`sector ${i}`} style={style.sectorName}><a onClick={this.props.setGraph}>{el[0]}</a></div>
              <div key={`change ${i}`} style={Number(el[1].split('%')[0]) > 0 ? style.price.positive : Number(el[1].split('%')[0]) < 0 ? style.price.negative : style.price}>{el[1]}</div>
            </div>
          )
        }, this) : <div style={style.each}><div style={style.price}>loading...</div></div>}
      </div>
    )

  }

}
/************************************************************************/
export default Sectors;
