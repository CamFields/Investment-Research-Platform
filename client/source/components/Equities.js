import React from 'react';
/************************************************************************/
class Equities extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {

    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '3/ span 3',
      gridColumn: '7',
      margin: '0px 5px 0px 0px',/*73px 5px 0px 0px*/
      heading: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        padding: '10px 0px 0px 10px',
        gridColumn: '1/ span 3',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'indianRed',
      },
      each: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        height: '50px',
        padding: '0px 10px 0px 10px',
      },
      symbol: {
        gridColumn: '1',
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'white',
        textDecoration: 'none',
      },
      price: {
        gridColumn: '3',
        alignSelf: 'center',
        justifySelf: 'right',
        color: 'grey',
        positive: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'mediumSpringGreen',
          fontWeight: 'bold',
        },
        negative: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'orangeRed',
          fontWeight: 'bold',
        },
      },

    }
/*---------------------------PARSING API DATA---------------------------*/
    let parseBatchEquities = (() => {
      let store = [];

      for (let each in this.props.tickers[0]) {
        store.push(this.props.tickers[0][each]["quote"])
      }

      return store;
    })();
/*----------------------------------------------------------------------*/
    return (
      <div className="Equities" style={style}>
        <div style={style.heading}>Equities</div>
          {parseBatchEquities.length > 0 ? parseBatchEquities.map((el, i) => {
            return (
              <div key={`${i}`} style={style.each}>
                <div key={`symbol ${i}`} style={style.symbol}><a style={style.symbol} onClick={this.props.setGraph}>{el["symbol"]}</a></div>
                <div key={`price ${i}`} style={el.latestPrice > el.previousClose ? style.price.positive : el.latestPrice < el.previousClose ? style.price.negative : style.price}>{parseFloat(el.latestPrice).toFixed(2)}</div>
              </div>
            )
          }) : <div style={style.each}><div style={style.price}>loading...</div></div>}
      </div>
    )

  }

}
/************************************************************************/
export default Equities;
