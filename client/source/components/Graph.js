import React from 'react';
import {Line} from 'react-chartjs-2';

/************************************************************************/
class Graph extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '1 / span 2',
      gridColumn: '1 / span 5',
      margin: '0px 30px 0px 0px',
      width: '100%',
      // height: '50px',
      // alignSelf: 'stretch',
      // justifySelf: 'stretch',
      // border: 'solid 2px red',

      heading: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        padding: '10px 0px 0px 10px',
        gridColumn: '1/ span 3',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        alignSelf: 'stretch',
        justifySelf: 'stretch',

      },
    }

    let options = {
      tooltips: {
        // mode: 'average',
        backgroundColor: 'black',
        titleFontColor: 'white',
        titleFontStyle: 'bold',
        titleFontSize: 14,
        bodyFontSize: 14,
        bodyFontColor: 'indianRed',
        bodyFontStyle: 'bold',
        // caretPadding: 10,
        // caretSize: 5,
        displayColors: false,
        borderColor: 'white',
        borderWidth: 1,
      },
      maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            display: true,
            gridLines: {
              display: true,
              color: 'white',
              tickMarkLength: 0,
            },
            ticks: {
              fontColor: 'white',
              fontSize: 12,
              padding: 10,
              labelOffset: -4,
            },
            // position: 'right',
          },
        ],
        xAxes: [
          {
            display: true,
            gridLines: {
              display: true,
              color: 'white',
              tickMarkLength: 0,
            },
            ticks: {
              fontColor: 'white',
              fontSize: 12,
              maxRotation: 67.5,
              minRotation: 67.5,
              labelOffset: -3,
              padding: 10,
              // display: true,
            },
          },
        ],
      },
      title: {
        display: true,
        text: `${this.props.current || 'BTC'} Price (USD - 30 Days)`,
        fontSize: 18,
        fontColor: 'white',
      },
      legend: {
        display: false,
        position: 'bottom',
      },
    };

    return (
      <div className="Graph" style={style}>
        <Line
          data={this.props.data}
          options={options}
        />
      </div>
    )
  }


}
/************************************************************************/

export default Graph;
