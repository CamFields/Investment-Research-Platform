import React from 'react';
/************************************************************************/
class Cryptocurrencies extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '3/ span 3',
      gridColumn: '6',
      // fontSize: '10px',
      margin: '0px 5px 0px 0px',/*73px 5px 0px 0px*/
      heading: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        padding: '10px 0px 0px 10px',
        gridColumn: '1/ span 3',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'indianRed',
      },
      each: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        height: '50px',
        padding: '0px 10px 0px 10px',
      },
      symbol: {
        gridColumn: '1',
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'white',
        textDecoration: 'none',
      },
      price: {
        gridColumn: '3',
        alignSelf: 'center',
        justifySelf: 'right',
        color: 'grey',
        positive: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'mediumSpringGreen',
          fontWeight: 'bold',
        },
        negative: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'orangeRed',
          fontWeight: 'bold',
        },
      },

    }
/*---------------------------PARSING API DATA---------------------------*/
    let parseCrypto = (() => {
      let arrayed = [],
          tickers = Object.keys(this.props.cryptoTickers[0]);

      for (let each of tickers) {
        arrayed.push([each, this.props.cryptoTickers[0][each]["USD"], this.props.cryptoTickersBaseline[0][each]["USD"]])
      }

      return arrayed;
    })()
/*----------------------------------------------------------------------*/
    return (
      <div className="Cryptocurrencies" style={style}>
        <div style={style.heading}>Crypto</div>
          {parseCrypto.map((el, i) => {
            return (
              <div key={`${i}`} style={style.each}>
                <div key={`ticker ${i}`} style={style.symbol}><a style={style.symbol} onClick={this.props.setGraph}>{el[0]}</a></div>
                <div key={`price ${i}`} style={Number(el[1]) > el[2] ? style.price.positive : Number(el[1]) < el[2] ? style.price.negative : style.price}>{el[1].toFixed(2)}</div>
              </div>
            )
          })}
      </div>
    )

  }

}
/************************************************************************/
export default Cryptocurrencies;
