import React from 'react';
/************************************************************************/
class RecentNews extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '3/ span 3',
      gridColumn: '1/ span 3',
      margin: '0px 5px 0px 5px',
      heading: {
        margin: '0px 0px 7px 0px',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        padding: '10px 0px 0px 10px',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'indianRed',
      },
      article: {
        border: 'solid 3px black',
        padding: '10px 10px 5px 10px',
        boxShadow: '2px 4px 3px black',
        margin: '0px 0px 7px 0px',
        // marginBottom: '10px',
        // marginLeft: '5px',
        title: {
          color: 'white',
          marginBottom: '5px',
          textDecoration: 'none',
          textDecorationStyle: 'single',
          fontWeight: 'bold',
          display: 'block',
        },
        author: {
          color: 'royalBlue',
          marginTop: '7px',
          fontWeight: 'lighter',
        },
        description: {
          textDecoration: 'none',
          color: 'grey',
        },
      },

    }

    return (
      <div className="RecentNews" style={style}>
        <div style={style.heading}>Recent News</div>
        {this.props.news.map(function(el, i) {
          return (
            <div key={`${i}`} style={style.article}>
              <a href={el.url} key={`title ${i}`} style={style.article.title}>{el.title}</a>
              <a href={el.url} key={`description ${i}`} style={style.article.description}>{el.description}</a>
              <div key={`author ${i}`} style={style.article.author}>{el.author}</div>
            </div>
          )
        })}
      </div>
    )

  }

}
/************************************************************************/
export default RecentNews;
