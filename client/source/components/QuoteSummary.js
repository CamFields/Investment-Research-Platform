import React from 'react';
/************************************************************************/
class QuoteSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '1/ span 2',
      gridColumn: '6/ span 2',
      // border: '2px solid red',
      margin: '0px 5px 0px 0px',
      // border: 'solid 1px white',
      paddingTop: '40px',
      paddingBottom: '60px',
      each: {
        display: 'grid',
        gridColumn: '1fr 1fr 1fr',
        padding: '0px 10px 0px 10px',
        borderBottom: 'solid 1px white',
      },
      property: {
        gridColumn: '1',
        alignSelf: 'center',
      },
      value: {
        gridColumn: '3',
        alignSelf: 'center',
        justifySelf: 'right',
      },

    }

    let currentData = (() => {
      if (this.props.data.change) {
        return (
          <div style={style.each}>
            <div style={style.property}>open</div>
            <div style={style.value}>{this.props.data.open}</div>
          </div>
        )
      }
    })()

    return (
      <div className="QuoteSummary" style={style}>
        <div>{currentData}</div>
      </div>
    )

  }

}
/************************************************************************/
export default QuoteSummary;
/*
text: `${this.props.current || 'BTC'} Price (USD - 30 Days)`,
current={this.state.selected}

display: 'grid',
border: 'solid 3px black',
boxShadow: '2px 4px 3px black',
gridColumn: '1fr 1fr 1fr',
padding: '10px 0px 0px 10px',
gridColumn: '1/ span 3',
height: '40px',
fontSize: '24px',
fontWeight: 'bold',
textAlign: 'center',
color: 'indianRed',
*/
