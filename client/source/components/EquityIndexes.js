import React from 'react';
/************************************************************************/
class EquityIndexes extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const style = {
      backgroundColor: 'rgb(27, 27, 29)',
      gridRow: '3/ span 3',
      gridColumn: '4',
      margin: '0px 5px 0px 0px',/*73px 5px 0px 0px*/
      heading: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        padding: '10px 0px 0px 10px',
        gridColumn: '1/ span 3',
        height: '40px',
        fontSize: '24px',
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'indianRed',
      },
      each: {
        display: 'grid',
        border: 'solid 3px black',
        boxShadow: '2px 4px 3px black',
        gridColumn: '1fr 1fr 1fr',
        height: '50px',
        padding: '0px 10px 0px 10px',
      },
      symbol: {
        gridColumn: '1',
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'white',
        textDecoration: 'none',
      },
      price: {
        gridColumn: '3',
        alignSelf: 'center',
        justifySelf: 'right',
        color: 'grey',
        positive: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'mediumSpringGreen',
          fontWeight: 'bold',
        },
        negative: {
          gridColumn: '3',
          alignSelf: 'center',
          justifySelf: 'right',
          color: 'orangeRed',
          fontWeight: 'bold',
        },
      },

    }


    return (
      <div className="Equity Indexes" style={style}>
        <div style={style.heading}>Indexes</div>
        {this.props.indexes.map((el, i) => {
            return (
                <div key={`${i}`} style={style.each}>
                  <div style={style.symbol}>{el['Global Quote']['01. symbol']}</div>
                  <div style={Number(el['Global Quote']['02. open']) > Number(el['Global Quote']['05. price']) ? style.price.positive : Number(el['Global Quote']['02. open']) < Number(el['Global Quote']['05. price']) ? style.price.negative : style.price}>{el['Global Quote']['05. price'] ? parseFloat(el['Global Quote']['05. price']).toFixed(2) : 'loading...'}</div>
                </div>
              )
            })}
      </div>

    )
  }

}
/************************************************************************/
export default EquityIndexes;
