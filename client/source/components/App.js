import React from 'react';
import EquityIndexes from './EquityIndexes';
import Sectors from './Sectors.js';
import Equities from './Equities.js';
import Cryptocurrencies from './Cryptocurrencies.js';
import RecentNews from './RecentNews';
import Graph from './Graph';
import QuoteSummary from './QuoteSummary.js';
import axios from 'axios';
import newsAPI from '../../../KEYS/news.js';
import alphaVantageAPI from '../../../KEYS/alphaVantage.js';
import cryptoCompareAPI from '../../../KEYS/cryptoCompare.js';
import iexAPI from '../../../KEYS/IEXCloud.js';
import Request from 'axios-request-handler';
/********************************API URLS********************************/
const batchEquitiesCall = new Request(
  `https://cloud.iexapis.com/stable/stock/market/batch?token=${iexAPI}&symbols=BTC,MSFT,FB,AAP,GOOG,BOA,JPM,BABA,SNAP,AMD,CSCO,TSLA,AMZN,WFC,BOA,TECH,BTAI,BHF,CHY&types=quote&range=1m&last=5`
);
const watchlistCryptocurrencyCall = new Request(
  `https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,EOS,LTC,BCH,USDT,TRX,XLM,BNB,BSV,ADA,XMR,MIOTA,DASH,NEO,MKR&tsyms=USD&api_key=${cryptoCompareAPI}`
);
const newsApiCall = new Request(
  `https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=${newsAPI}`
);
const sectorsCall = new Request(
  `https://www.alphavantage.co/query?function=SECTOR&apikey=${alphaVantageAPI}`
);
/*---------------------------------------------------------------------*/
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      news: [{}],
      tickers: [{}],
      sectors: [{"Rank A: Real-Time Performance": {}}],
      cryptoTickers: [{}],
      cryptoTickersBaseline: [{}],
      indexes: [{"Global Quote": {}}],
      selected: '',
      selectedSummary: {},
      data: {},
    }
  }

  handleClicked(e) {
    let temp = this.state.selected;
    this.setState({
      // Retrieve a passed parameter 'value' attribute
      selected: e.target.text,
    });
    this.setGraph(e.target.text, temp);
  }

  setGraph(selected, temp) { /*https://iexcloud.io/docs/api/#historical-prices*/
    axios.get(`https://cloud.iexapis.com/stable/stock/${selected}/chart?token=${iexAPI}&range=3m&includeToday=true`)
      .then((response) => {
        if (response.data.length > 0) {
          let parseDates = (() => {
            let dates = response.data.map((el,i) => {
              return el['date'].split('-')[1] + '-' + el['date'].split('-')[2];
            })
            return dates.slice(-30)
          })();

          let parsePrices = (() => {
            let prices = [];
            for (var day of response.data) {
              prices.push(parseFloat(day['close']).toFixed(2))
            }
            return prices.slice(-30)
          })();

          this.setState({
            data: {
              labels: parseDates,
              datasets: [{
                // label: 'Price',
                data: parsePrices,
                backgroundColor: 'rgba(0, 0, 0, 0.95)', //fill color under line
                borderColor: 'indianRed' , //color of line
                borderWidth: 3,
                borderCapStyle: 'butt',
                borderJoinStyle: 'miter',
                pointRadius: 0,
                pointHitRadius: 30,
                pointStyle: 'circle',
                pointBorderColor: 'white',
                tension: 0,
                pointBackgroundColor: 'white',
                pointHoverBorderWidth: 4,
              }],
            },
            selectedSummary: response.data.slice(-1)[0],
          });

        }
      })
      .catch((error) => {
        this.setState({
          selected: temp,
        });
        console.log('ERROR fetching data iexAPI (setGraph)', error);
        alert('Sorry, Historic Data is not available for selected quote')
      });

  }
/*--------------------------BASELINE API CALLS--------------------------*/
  componentWillMount() {
    axios.get(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,XRP,EOS,LTC,BCH,USDT,TRX,XLM,BNB,BSV,ADA,XMR,MIOTA,DASH,NEO,MKR&tsyms=USD&api_key=${cryptoCompareAPI}`)
      .then((response) => {
        if (response.data) {
          this.setState({
            cryptoTickersBaseline: [response.data],
          });
        }
      })
      .catch((error) => {
        console.log('API call for baseline cryptocurrencies :: ERROR', error);
      });

    axios.get(`https://api.coindesk.com/v1/bpi/historical/close.json`)
      .then((response) => {
        let parseDates = Object.keys(response.data.bpi).map((el) => {
          return el.split('-')[1] + '-' + el.split('-')[2]/*+ '/' + el.split('-')[0].slice(0,2)*/;
        })

        this.setState({
          data: {
            labels: parseDates,
            datasets: [{
              // label: 'Price',
              data: Object.values(response.data.bpi),
              backgroundColor: 'rgba(0, 0, 0, 0.95)', //fill color under line
              borderColor: 'indianRed' , //color of line
              borderWidth: 3,
              borderCapStyle: 'butt',
              borderJoinStyle: 'miter',
              pointRadius: 0,
              pointHitRadius: 30,
              pointStyle: 'circle',
              pointBorderColor: 'white',
              tension: 0,
              pointBackgroundColor: 'white',
              pointHoverBorderWidth: 4,
            }],
          },
        });
      })
      .catch((error) => {
        console.log('ERROR fetching data from coin desk api', error);
      });
/*--------------------------INDEXES API CALLS---------------------------*/
    (async () => {
      let currentIndexes = [];

      try {
        const djia = await axios.get(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=DJIA&apikey=${alphaVantageAPI}`)
        if (djia.data.hasOwnProperty('Global Quote')) {
          currentIndexes.push(djia.data);
        }
        const ndaq = await axios.get(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=NDAQ&apikey=${alphaVantageAPI}`)
        if (ndaq.data.hasOwnProperty('Global Quote')) {
          currentIndexes.push(ndaq.data);
        }
        const spx = await axios.get(`https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=SPX&apikey=${alphaVantageAPI}`)
        if (spx.data.hasOwnProperty('Global Quote')) {
          currentIndexes.push(spx.data);
        }
      } catch (error) {
        console.error('API Call for Indexes ERROR:', error);
      }

      if (currentIndexes.length > 0) {
        this.setState({
          indexes: currentIndexes,
        });
      }
    })();

  }
/*-----------------------------POLLING APIS-----------------------------*/
  componentDidMount() {
    newsApiCall.poll(2400000).get((response) => {
      this.setState({
        news: response.data.articles,
      });
    })
    .catch((error) => {
      console.log('API Call to alphaVantageAPI :: ERROR', error);
      return false; // polling can be canceled by returning false
    });

    batchEquitiesCall.poll(60000).get((response) => {
      if (response.data) {
        console.log(response.data)
        // this.setState({
        //   tickers: [response.data],
        // })
      }
    })
    .catch((error) => {
      console.log('Batch Equities API Call ERROR:', error);
      return false; // polling can be canceled by returning false
    });
//ISSUE HERE IN SECTOR CALL: failing intermittenly
    sectorsCall.poll(300000).get((response) => {
      if (response.data.hasOwnProperty("Rank A: Real-Time Performance")) {
        this.setState({
          sectors: [response.data],
        })
      }
    })
    .catch((error) => {
      console.log('API Call to alphaVantageAPI :: ERROR', error);
      return false; // polling can be canceled by returning false
    });

    watchlistCryptocurrencyCall.poll(60000).get((response) => {
      if (Object.keys(this.state.cryptoTickersBaseline[0]).length >= 1) {
        this.setState({
          cryptoTickers: [response.data],
        })
      } else {
        console.log('error in crypto call')
      }
    })
    .catch((error) => {
      console.log('API Call to cryptoCompare :: ERROR', error);
      return false; // polling can be canceled by returning false
    });

  }
/*----------------------------------------------------------------------*/
  render() {
    const style = {
      display: 'grid',
      backgroundColor: 'rgb(27, 27, 29)',
      color: 'white',
      fontFamily: 'Arial, Helvetica, DIN Pro, -apple-system, BlinkMacSystemFont, sans-serif',
      margin: '-8px -8px -8px -8px',
      gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr 1fr',
      gridTemplateRows: '250px 250px 1fr 1fr 1fr 1fr',
      justifyContent: "stretch",
      gridAutoFlow: 'row',
      gridAutoFlow: 'column',
    }

    return (
      <div className="App" style={style}>
        <Graph current={this.state.selected} data={this.state.data}/>
        <EquityIndexes indexes={this.state.indexes}/>
        <Sectors setGraph={this.handleClicked.bind(this)} sectors={this.state.sectors} setGraph={this.handleClicked.bind(this)}/>
        <Equities setGraph={this.handleClicked.bind(this)} tickers={this.state.tickers}/>
        <Cryptocurrencies setGraph={this.handleClicked.bind(this)} cryptoTickers={this.state.cryptoTickers} cryptoTickersBaseline={this.state.cryptoTickersBaseline}/>
        <RecentNews news={this.state.news}/>
        <QuoteSummary current={this.state.selected} data={this.state.selectedSummary}/>
      </div>
    )

  }

}
/************************************************************************/
export default App;
